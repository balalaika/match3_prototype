﻿namespace Match3Prototype
{
    public enum FieldConstraintsType : byte
    {
        Top,
        Bot,
        Right,
        Left
    }
}