﻿using System;
using TMPro;
using UnityEngine;

namespace Match3Prototype
{
    [RequireComponent(typeof(SpriteRenderer), typeof(Rigidbody2D))]
    public class Match3Tile : MonoBehaviour
    {
        [SerializeField] private TextMeshPro _textOnTile;
        public TextMeshPro TextOnTile => _textOnTile;

        [SerializeField] private float _velocityIntendForMovingFlag = 0.15f;

        private bool _isMoving = false;

        /// <summary>
        /// Invoking after state of _isMoving changes.
        /// </summary>
        public Action<bool> OnIsMovingStateChange;
    
        private Color _color;

        public Color Color
        {
            set
            {
                _color = value;

                _sprite.color = _color;
            }

            get
            {
                return _color;
            }
        }

        private IntVector2 _arrayPosition;

        public IntVector2 ArrayPosition
        {
            set
            {
                _arrayPosition = value;
                UpdateText();
            }

            get
            {
                return _arrayPosition;
            }
        }

        private TileTypes _tileType = TileTypes.Regular;
        public TileTypes TileType => _tileType;

        public void ResetType()
        {
            _tileType = TileTypes.Regular;
            UpdateText();
        }

        public void SetType(TileTypes newType)
        {
            _tileType = newType;
            UpdateText();
        }

        private SpriteRenderer _sprite;
        private Rigidbody2D _body;

        private void Awake()
        {
            _sprite = GetComponent<SpriteRenderer>();
            _body = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            if (_body.IsSleeping()) return;
            
            var nowIsMoving = Mathf.Abs(_body.velocity.y) > _velocityIntendForMovingFlag;

            if (nowIsMoving && _isMoving) return;
            if (!nowIsMoving && !_isMoving) return;

            if (!nowIsMoving && _isMoving)
                _isMoving = false;

            if (nowIsMoving && !_isMoving)
                _isMoving = true;
            
            OnIsMovingStateChange?.Invoke(_isMoving);
        }

        private void UpdateText()
        {
            _textOnTile.text = $"{_tileType}{Environment.NewLine}[{_arrayPosition.X}:{_arrayPosition.Y}]";
        }

        private void OnDestroy()
        {
            OnIsMovingStateChange = null;
        }
    }
}