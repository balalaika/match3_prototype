﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;

namespace Match3Prototype
{
    public class GameField : MonoBehaviour
    {
        [Header("Setups")] 
        // todo Вынести в ScriptableObject
        [SerializeField] private IntVector2 _size;
        [SerializeField, Range(1, 5)] private int _tileStep = 1;
        [SerializeField, Range(2, 10)] private int _minCountOfTileForMatch = 3;
        [SerializeField, Range(2, 10)] private int _minCountOfTileForSpecial = 4;
        [SerializeField] private TileColors _tileColors;
        [SerializeField] private Match3Tile _tilePrefab;

        [Header("References")] [SerializeField]
        private ObjectsPool _poolOfTiles;
        [SerializeField] private GameFieldConstraints _gameFieldConstraints;
        public GameFieldConstraints GameFieldConstraints => _gameFieldConstraints;

        private Match3Tile[,] _tilesInField;

        public UnityEvent OnSpecialTileMatched;

        private int _countOfMovingTiles;
        public int CountOfMovingTiles => _countOfMovingTiles;

        private FieldStates _fieldState;

        public FieldStates FieldState
        {
            get => _fieldState;
            set
            {
                _fieldState = value;
                OnStateChanged?.Invoke(_fieldState);
            }
        }

        public Action<FieldStates> OnStateChanged;

        private void Start()
        {
            FieldState = FieldStates.FieldIsEmpty;
            
            _gameFieldConstraints.FieldSize = _size.ToVector2();
            
            _poolOfTiles.AddToPool(_size.X * _size.Y, 
                _tilePrefab.gameObject, 
                Vector3.zero, 
                Quaternion.identity, 
                transform,
                o =>
                {
                    var match3TileScript = o.GetComponent<Match3Tile>();
                    match3TileScript.OnIsMovingStateChange +=
                        isMoving =>
                        {
                            if (isMoving) _countOfMovingTiles++;
                            else _countOfMovingTiles--;
                        };
                });
        }
        
        private Vector3 GetIntend(GravityDirection gravityDirection = GravityDirection.NormalGravity)
        {
            if (gravityDirection == GravityDirection.NormalGravity)
                return transform.position + new Vector3(-_size.X / 2.0f + 0.5f, _size.Y / 2.0f - 0.5f);
            
            return transform.position - new Vector3(_size.X / 2.0f - 0.5f, _size.Y / 2.0f - 0.5f);
        }
            
        /// <summary>
        /// Полностью перегенирирует поле.
        /// </summary>
        public void GenerateNew()
        {
            if (FieldState != FieldStates.Idle && FieldState != FieldStates.FieldIsEmpty) return;
            if (_countOfMovingTiles > 0) return;
            
            var tilesOnField = transform.GetComponentsInChildren<Match3Tile>();

            if (_poolOfTiles.UsedObjects.Count > 0)
                foreach (var tile in tilesOnField)
                    _poolOfTiles.Return(tile.gameObject);

            var intend = GetIntend();
            
            _tilesInField = new Match3Tile[_size.X, _size.Y];
            
            for (var x = 0; x < _size.X; x++)
            {
                for (var y = 0; y < _size.Y; y++)
                {
                    var newTile = _poolOfTiles.Get(
                        intend + new Vector3(x * _tileStep, -y * _tileStep));

                    ProceedNewTile(newTile, x, y);
                }
            }

            StartCoroutine(nameof(CheckAllTiles));
        }

        private void ProceedNewTile(GameObject newTile, int x, int y)
        {
            var newTileScript = newTile.GetComponent<Match3Tile>();
            newTileScript.ArrayPosition = new IntVector2(x, y);
            newTileScript.Color = _tileColors.GetRandomColor();
            newTileScript.ResetType();

            _tilesInField[x, y] = newTileScript;
        }

        private IEnumerator WaitUntilFieldNotIdleOrTilesMoving(Action actionAfter)
        {
            while (_countOfMovingTiles > 0 || _fieldState != FieldStates.Idle)
            {
                yield return new WaitForFixedUpdate();
            }
            
            actionAfter?.Invoke();
        }

        private IEnumerator CheckAllTiles()
        {
            FieldState = FieldStates.CheckTilesMatches;

            var isHaveMatch = false;

            for (var x = 0; x < _size.X; x++)
            {
                for (var y = 0; y < _size.Y; y++)
                {
                    // Ждем пока все тайлы встанут на свои места.
                    while (_countOfMovingTiles > 0)
                    {
                        yield return new WaitForFixedUpdate();
                    }

                    if (isHaveMatch) break;

                    // Проверяем на матчи.
                    CheckTileMatch(_tilesInField[x, y], out isHaveMatch);

                    // Дадим тайлам подвинуться.
                    yield return new WaitForFixedUpdate();
                }
                
                // Необходимо делать перепроверку, так как там где уже прошли снова могут быть матчи.
                if (isHaveMatch)
                {
                    x = 0;
                    isHaveMatch = !isHaveMatch;
                }
            }

            FieldState = FieldStates.Idle;
        }

        private void CheckTileMatch(Match3Tile tileForMatch, out bool isHaveMatch)
        {
            var matchedForTile1InPositionTile2 = 
                TileMatcher.TryMatchTileForNewPosition(
                    tileForMatch, 
                    tileForMatch.ArrayPosition, 
                    tileForMatch.Color,
                    _size, 
                    _minCountOfTileForMatch, 
                    _tilesInField);

            if (matchedForTile1InPositionTile2.Length == 0)
            {
                isHaveMatch = false;
                return;
            }

            // Уничтожаем всё что сматчилось.
            foreach (var tPos in matchedForTile1InPositionTile2)
                ProceedMatchedTile(tPos);
            
            // Обновляем позиции тайлов.
            for (var x = 0; x < _size.X; x++)
                for (var y = 0; y < _size.Y; y++)
                {
                    if (_tilesInField[x, y] != null) continue;
                            
                    RefreshTilePositions(new IntVector2(x, y));
                }

            GenerateNewTile();

            isHaveMatch = true;
            
            Debug.Log("Automatic matches found!");
        }

        private void ProceedMatchedTile(IntVector2 tilePos)
        {
            var tile = _tilesInField[tilePos.X, tilePos.Y];

            if (tile.TileType == TileTypes.Special)
            {
                tile.SetType(TileTypes.SpecialCharged);
            }
            else
            {
                if (tile.TileType == TileTypes.SpecialCharged)
                {
                    tile.SetType(TileTypes.Regular);

                    StartCoroutine(WaitUntilFieldNotIdleOrTilesMoving(() =>
                    {
                        OnSpecialTileMatched?.Invoke();
                    }));
                }
                    
                _poolOfTiles.Return(tile.gameObject);
                _tilesInField[tilePos.X, tilePos.Y] = null;
            }
        }
        
        public void MatchTilesFor(Match3Tile tile1, Match3Tile tile2)
        {
            FieldState = FieldStates.CheckUserInputMatches;
            
            Action<Match3Tile, Match3Tile> swapTile = (Match3Tile fTile, Match3Tile sTile) =>
            {
                var tile2Pos = sTile.transform.position;
                sTile.transform.position = fTile.transform.position;
                fTile.transform.position = tile2Pos;
            
                var tile2PosInArr = sTile.ArrayPosition;
                sTile.ArrayPosition = fTile.ArrayPosition;
                fTile.ArrayPosition = tile2PosInArr;
            
                _tilesInField[sTile.ArrayPosition.X, sTile.ArrayPosition.Y] = sTile;
                _tilesInField[fTile.ArrayPosition.X, fTile.ArrayPosition.Y] = fTile;
            };
            
            // Пробуем сматчить на новую позицию.
            var matchedForTile1InPositionTile2 = 
                TileMatcher.TryMatchTileForNewPosition(
                    tile1, 
                    tile2.ArrayPosition, 
                    tile2.Color,
                    _size, 
                    _minCountOfTileForMatch, 
                    _tilesInField);
            
            var matchedForTile2InPositionTile1 = 
                TileMatcher.TryMatchTileForNewPosition(
                    tile2, 
                    tile1.ArrayPosition, 
                    tile1.Color,
                    _size, 
                    _minCountOfTileForMatch, 
                    _tilesInField);

            var countOfMatched = matchedForTile1InPositionTile2.Length + matchedForTile2InPositionTile1.Length;
            
            if (countOfMatched == 0) return;

            swapTile(tile1, tile2);
            
            // Special tile.
            if (countOfMatched >= _minCountOfTileForSpecial)
            {
                var randomIndex = UnityEngine.Random.Range(0, countOfMatched);
                var specialPosition = randomIndex < matchedForTile1InPositionTile2.Length
                    ? matchedForTile1InPositionTile2[randomIndex]
                    : matchedForTile2InPositionTile1[randomIndex - matchedForTile1InPositionTile2.Length];
                _tilesInField[specialPosition.X, specialPosition.Y].SetType(TileTypes.Special);
            }

            // Уничтожаем всё что сматчилось.
            foreach (var tPos in matchedForTile1InPositionTile2)
                ProceedMatchedTile(tPos);

            foreach (var tPos in matchedForTile2InPositionTile1)
                ProceedMatchedTile(tPos);
            
            // Обновляем позиции тайлов.
            for (var x = 0; x < _size.X; x++)
                for (var y = 0; y < _size.Y; y++)
                {
                    if (_tilesInField[x, y] != null) continue;
                        
                    RefreshTilePositions(new IntVector2(x, y));
                    //break;
                }
            
            GenerateNewTile();

            FieldState = FieldStates.Idle;

            StartCoroutine(nameof(CheckAllTiles));
            
            Debug.Log("User matches found!");
        }

        private void RefreshTilePositions(IntVector2 startPosition)
        {
            var y = startPosition.Y;

            if (GameManager.Instance.GravityDirection == GravityDirection.NormalGravity)
            {
                if (startPosition.Y == 0) return;

                while (y != 0)
                {
                    if (_tilesInField[startPosition.X, y - 1] != null)
                        _tilesInField[startPosition.X, y - 1].ArrayPosition = new IntVector2(startPosition.X, y);

                    _tilesInField[startPosition.X, y] = _tilesInField[startPosition.X, y - 1];
                    y--;
                }

                _tilesInField[startPosition.X, 0] = null;
            }
            else
            if (GameManager.Instance.GravityDirection == GravityDirection.ReverseGravity)
            {
                if (startPosition.Y == _size.Y - 1) return;
                
                while (y < _size.Y - 1)
                {
                    if (_tilesInField[startPosition.X, y + 1] != null)
                        _tilesInField[startPosition.X, y + 1].ArrayPosition = new IntVector2(startPosition.X, y);

                    _tilesInField[startPosition.X, y] = _tilesInField[startPosition.X, y + 1];
                    y++;
                }

                _tilesInField[startPosition.X, _size.Y - 1] = null;
            }
        }

        private void GenerateNewTile()
        {
            var intend = GetIntend(GameManager.Instance.GravityDirection);
            var isNormalGravity = GameManager.Instance.GravityDirection == GravityDirection.NormalGravity;
            
            for (var x = 0; x < _size.X; x++)
                for (var y = 0; y < _size.Y; y++)
                {
                    if (_tilesInField[x, y] != null) continue;
                    
                    var offsetVector = isNormalGravity
                        ? new Vector3(x * _tileStep, _size.Y - y * _tileStep)
                        : new Vector3(x * _tileStep, - (_size.Y + y * _tileStep));
                        
                    var newPos = intend + offsetVector;
                    
                    var newTile = _poolOfTiles.Get(newPos);
                    
                    ProceedNewTile(newTile, x, y);
                }
        }
    }
}