﻿using UnityEngine;

namespace Match3Prototype
{
    [CreateAssetMenu(fileName = "TileColors", menuName = "TileColors", order = 0)]
    public class TileColors : ScriptableObject
    {
        public Color[] Colors;

        public Color GetRandomColor()
        {
            return Colors[Random.Range(0, Colors.Length)];
        }
    }
}