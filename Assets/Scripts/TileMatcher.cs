﻿using System.Collections.Generic;
using UnityEngine;

namespace Match3Prototype
{
    public static class TileMatcher
    {
        public static IntVector2[] TryMatchTileForNewPosition(
            Match3Tile tile, 
            IntVector2 newPosition,
            Color newColor,
            IntVector2 size, 
            int minCountForMatch,
            Match3Tile[,] tilesInField)
        {
            if (!IsHaveCloserMatch(tile, newColor, newPosition)) return new IntVector2[0];
            
            var matchedHorizontal = new Queue<IntVector2>();
            var matchedVertical = new Queue<IntVector2>();
            
            // Go left
            if (newPosition.X > 0)
            {
                for (var x = newPosition.X - 1; x >= 0; x--)
                {
                    var tileToCheck = tilesInField[x, newPosition.Y];
                    var tileColor = tileToCheck == tile ? newColor : tileToCheck.Color;

                    if (tileColor != tile.Color) break;
                    
                    matchedHorizontal.Enqueue(tileToCheck.ArrayPosition);
                }
            }
            
            // Go right
            if (newPosition.X < size.X)
            {
                for (var x = newPosition.X + 1; x < size.X; x++)
                {
                    var tileToCheck = tilesInField[x, newPosition.Y];
                    var tileColor = tileToCheck == tile ? newColor : tileToCheck.Color;

                    if (tileColor != tile.Color) break;
                    
                    matchedHorizontal.Enqueue(tileToCheck.ArrayPosition);
                }
            }
            
            // Если суммарно влево и вправо не набралось на минимальное количество для матча, то не матчим их.
            // +1 за наш тайл
            if (matchedHorizontal.Count + 1 < minCountForMatch) matchedHorizontal.Clear();
            
            // Go Top
            if (newPosition.Y < size.Y)
            {
                for (var y = newPosition.Y + 1; y < size.Y; y++)
                {
                    var tileToCheck = tilesInField[newPosition.X, y];
                    var tileColor = tileToCheck == tile ? newColor : tileToCheck.Color;

                    if (tileColor != tile.Color) break;
                    
                    matchedVertical.Enqueue(tileToCheck.ArrayPosition);
                }
            }
            
            // Go Down
            if (newPosition.Y > 0)
            {
                for (var y = newPosition.Y - 1; y >= 0; y--)
                {
                    var tileToCheck = tilesInField[newPosition.X, y];
                    var tileColor = tileToCheck == tile ? newColor : tileToCheck.Color;

                    if (tileColor != tile.Color) break;
                    
                    matchedVertical.Enqueue(tileToCheck.ArrayPosition);
                }
            }
            
            // Если суммарно влево и вправо не набралось на минимальное количество для матча, то не матчим их.
            // +1 за наш тайл
            if (matchedVertical.Count + 1 < minCountForMatch) matchedVertical.Clear();
            
            if (matchedHorizontal.Count == 0 && matchedVertical.Count == 0) return new IntVector2[0];

            var matched = new IntVector2[matchedHorizontal.Count + matchedVertical.Count + 1];
            matched[matched.Length - 1] = newPosition;
            
            matchedHorizontal.CopyTo(matched, 0);
            matchedVertical.CopyTo(matched, matchedHorizontal.Count);

            return matched;
        }

        public static bool IsHaveCloserMatch(Match3Tile tile, Color newColor, IntVector2 newPosition)
        {
            //todo Если рядом на соседних клетках цвета разные, то проверять дальше нет смысла.

            return true;
        }
    }
}