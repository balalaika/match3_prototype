﻿using UnityEngine;

namespace Match3Prototype
{
    public static class GravityDirectionExtension
    {
        public static Vector2 ToVector2(this GravityDirection gravityDirection)
        {
            return gravityDirection == GravityDirection.NormalGravity ? Vector2.down : Vector2.up;
        }
    }
}