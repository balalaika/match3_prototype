﻿using System;
using UnityEngine;

namespace Match3Prototype
{
    public class GameFieldConstraints : MonoBehaviour
    {
        [SerializeField] private BoxCollider2D _topWall;
        [SerializeField] private BoxCollider2D _botWall;
        [SerializeField] private BoxCollider2D _leftWall;
        [SerializeField] private BoxCollider2D _rightWall;

        private Vector2 _fieldSize;
        public Vector2 FieldSize
        {
            set
            {
                _fieldSize = value;
                SetWallPerimeter();
            }
        }

        private BoxCollider2D GetWallByType(FieldConstraintsType type)
        {
            switch (type)
            {
                case FieldConstraintsType.Bot: return _botWall;
                case FieldConstraintsType.Top: return _topWall;
                case FieldConstraintsType.Right: return _rightWall;
                case FieldConstraintsType.Left: return _leftWall;
            }

            return null;
        }

        private void SetWallPerimeter()
        {
            var fieldHalfSize = _fieldSize / 2.0f;
            
            _topWall.transform.position = new Vector2(0, fieldHalfSize.y + _topWall.size.y / 2f);
            _botWall.transform.position = new Vector2(0, -fieldHalfSize.y - _botWall.size.y / 2f);

            _leftWall.transform.position = new Vector2(-fieldHalfSize.x - _leftWall.size.x / 2f, 0);
            _rightWall.transform.position = new Vector2(fieldHalfSize.x + _rightWall.size.x / 2f, 0);
            
            // todo Check component SpriteRenderer is not null
            _topWall.GetComponent<SpriteRenderer>().size = new Vector2(_fieldSize.x * 2.0f, _topWall.size.y);
            _botWall.GetComponent<SpriteRenderer>().size = new Vector2(_fieldSize.x * 2.0f, _botWall.size.y);

            _leftWall.GetComponent<SpriteRenderer>().size = new Vector2(_leftWall.size.x, _fieldSize.y * 2.0f);
            _rightWall.GetComponent<SpriteRenderer>().size = new Vector2(_rightWall.size.x, _fieldSize.y * 2.0f);
        }

        public void Open(FieldConstraintsType type)
        {
            GetWallByType(type).gameObject.SetActive(false);
        }

        public void Close(FieldConstraintsType type)
        {
            GetWallByType(type).gameObject.SetActive(true);
        }

        public void Close()
        {
            _topWall.gameObject.SetActive(true);
            _botWall.gameObject.SetActive(true);
            _leftWall.gameObject.SetActive(true);
            _rightWall.gameObject.SetActive(true);
        }
    }
}