﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Match3Prototype
{
    public class UserInput : MonoBehaviour
    {
        [SerializeField] private float _raycastLength = 10f;
        [SerializeField] private LayerMask _mask;

        public Action<Transform> OnHit;

        private void Update()
        {
            if (!Input.GetMouseButtonDown(0)) return;

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            var rayHit = Physics2D.GetRayIntersection(ray, _raycastLength, _mask);

            if (!rayHit) return;
            if (!rayHit.transform) return;

            OnHit?.Invoke(rayHit.transform);
        }

        private void OnDestroy()
        {
            OnHit = null;
        }
    }
}