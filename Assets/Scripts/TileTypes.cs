﻿namespace Match3Prototype
{
    public enum TileTypes : byte
    {
        Regular = 0,
        Special,
        SpecialCharged
    }
}