﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoSingletone<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;
    public static T Instance => _instance;

    public void SetInstance()
    {
        _instance = GetComponent<T>();
    }

    public void ResetInstance()
    {
        _instance = null;
    }
}
