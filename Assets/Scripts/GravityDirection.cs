﻿namespace Match3Prototype
{
    public enum GravityDirection : byte
    {
        NormalGravity = 0,
        ReverseGravity = 1
    }
}