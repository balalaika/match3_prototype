﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Match3Prototype
{
    public class ObjectsPool : MonoBehaviour
    {
        private List<GameObject> _pool = new List<GameObject>();

        private List<GameObject> _usedObjects = new List<GameObject>();
        public List<GameObject> UsedObjects => _usedObjects;

        public void AddToPool(int size, 
            GameObject prefab,
            Vector3 position, 
            Quaternion quaternion, 
            Transform parent,
            Action<GameObject> extendetActions = null)
        {
            for (var i = 0; i < size; i++)
            {
                var newObject = Instantiate(prefab, 
                    position, 
                    quaternion, 
                    parent);

                extendetActions?.Invoke(newObject);
                
                newObject.SetActive(false);
                _pool.Add(newObject);
            }
        }

        public GameObject Get()
        {
            return Get(Vector3.zero);
        }

        public GameObject Get(Vector3 position)
        {
            if (_pool.Count == 0) return null;
            
            var gameObject = _pool[Random.Range(0, _pool.Count)];
            
            _pool.Remove(gameObject);
            _usedObjects.Add(gameObject);

            gameObject.transform.position = position;
            gameObject.SetActive(true);
            
            return gameObject;
        }

        public void Return(GameObject gameObject)
        {
            if (_usedObjects.Count == 0) return;
            
            _usedObjects.Remove(gameObject);
            _pool.Add(gameObject);
            
            gameObject.SetActive(false);
        }
    }
}