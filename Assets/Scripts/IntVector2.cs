﻿using System;
using UnityEngine;

namespace Match3Prototype
{
    [Serializable]
    public struct IntVector2
    {
        [SerializeField] private int _x;
        [SerializeField] private int _y;

        public int X => _x;
        public int Y => _y;

        public IntVector2(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public Vector2 ToVector2()
        {
            return new Vector2(_x, _y);
        }
    }
}