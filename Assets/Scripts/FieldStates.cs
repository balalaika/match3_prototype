﻿namespace Match3Prototype
{
    public enum FieldStates : byte
    {
        FieldIsEmpty,
        Idle,
        CheckTilesMatches,
        CheckUserInputMatches
    }
}