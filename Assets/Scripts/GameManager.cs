﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Match3Prototype
{
    public class GameManager : MonoSingletone<GameManager>
    {
        private GravityDirection _gravityDirection = GravityDirection.NormalGravity;
        public GravityDirection GravityDirection => _gravityDirection;
        
        [SerializeField] private int _distanceToCheckMatchAllowed = 1;

        [Header("UI references")] [SerializeField]
        private Text _gameFieldText;
        
        [Header("References")]
        [SerializeField] private GameField _gameField;
        [SerializeField] private UserInput _userInput;

        private Match3Tile _firstSelectedTile;
        private Match3Tile _secondSelectedTile;

        private void Awake()
        {
            SetInstance();
        }

        private void OnDestroy()
        {
            ResetInstance();
        }
        
        public void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void Start()
        {
            CheckGravityDirection();

            _gameField.OnStateChanged += newState =>
            {
                _gameFieldText.text = newState.ToString();
            };

            _userInput.OnHit += (Transform transform) =>
            {
                if (_gameField.CountOfMovingTiles > 0) return;
                if (_gameField.FieldState != FieldStates.Idle) return;
                
                if (!_firstSelectedTile)
                {
                    _firstSelectedTile = transform.GetComponent<Match3Tile>();
                    Debug.Log($"Select first tile: {_firstSelectedTile.ArrayPosition} color {_firstSelectedTile.Color}");
                    return;
                }
                
                if (!_secondSelectedTile)
                {
                    if (_firstSelectedTile.transform == transform) return;
                    
                    _secondSelectedTile = transform.GetComponent<Match3Tile>();
                    Debug.Log($"Select second tile: {_secondSelectedTile.ArrayPosition} color {_secondSelectedTile.Color}");

                    var distanceBetweenTile =
                        new Vector2(_secondSelectedTile.ArrayPosition.X - _firstSelectedTile.ArrayPosition.X,
                            _secondSelectedTile.ArrayPosition.Y - _firstSelectedTile.ArrayPosition.Y).magnitude;
                    
                    if (distanceBetweenTile <= _distanceToCheckMatchAllowed)
                        _gameField.MatchTilesFor(_firstSelectedTile, _secondSelectedTile);
                    else 
                        Debug.Log("Tiles too far!");
                    
                    _firstSelectedTile = null;
                    _secondSelectedTile = null;
                }
            };
        }

        private void CheckGravityDirection()
        {
            if (Math.Sign(Physics2D.gravity.y) > 0 && _gravityDirection != GravityDirection.ReverseGravity)
            {
                Physics2D.gravity = new Vector2(Physics2D.gravity.x, Physics2D.gravity.y * -1);
                _gameField.GameFieldConstraints.Close(FieldConstraintsType.Bot);
                _gameField.GameFieldConstraints.Open(FieldConstraintsType.Top);

                return;
            }
            else 
            if (Math.Sign(Physics2D.gravity.y) < 0 && _gravityDirection != GravityDirection.NormalGravity)
            {
                Physics2D.gravity = new Vector2(Physics2D.gravity.x, Physics2D.gravity.y * -1);
                _gameField.GameFieldConstraints.Close(FieldConstraintsType.Top);
                _gameField.GameFieldConstraints.Open(FieldConstraintsType.Bot);

                return;
            }
            
            _gameField.GameFieldConstraints.Close();
            _gameField.GameFieldConstraints.Open(_gravityDirection == GravityDirection.NormalGravity ?
                                            FieldConstraintsType.Top : FieldConstraintsType.Bot);
        }

        public void ChangeGravityDirection()
        {
            _gravityDirection = _gravityDirection == GravityDirection.NormalGravity
                ? GravityDirection.ReverseGravity
                : GravityDirection.NormalGravity;
            
            CheckGravityDirection();
        }
    }
}